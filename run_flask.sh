#!/bin/sh

export UI_WORKERS="${UI_WORKERS:-1}"
export UI_THREADS="${UI_THREADS:-50}"
export REST_WORKERS="${REST_WORKERS:-1}"
export REST_THREADS="${REST_THREADS:-4}"

gunicorn -b 0.0.0.0:8000 app:app --workers=$UI_WORKERS --threads=$UI_THREADS --timeout 8000 --log-level debug &
gunicorn -b 0.0.0.0:8001 app_rest:app_rest --workers=$REST_WORKERS --threads=$REST_THREADS --timeout 8000 --log-level debug
