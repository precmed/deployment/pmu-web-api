apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pmu-web-app-storage
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 50Gi
  storageClassName: ceph-ext4
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: pmu-web-app
spec:
  selector:
    matchLabels:
      app: pmu-web-app
  replicas: 1
  template:
    metadata:
      labels:
        app: pmu-web-app
    spec:
      containers:
      - name: pmu-web-app
        image: registry.gitlab.com/precmed/deployment/pmu-web-api:3.0.2
        imagePullPolicy: Always
        env:
        - name: VAST_URL
          value: "vast-test.oncopmnet.gr"
        - name: S3_GATEWAY
          value: "rook-ceph-rgw-my-store.rook-ceph"
        - name: CROMWELL_PORT
          value: "8000"
        - name: OIDC_SECRET
          valueFrom:
            secretKeyRef:
              name: pmu-web-api-iodc-secrets
              key: OIDC_SECRET
        - name: IODC_CLIENT_SECRETS_PATH
          value: /etc/iodc_secrets/client_secrets.json
        - name: KEYCLOAK_URL
          value: "http://keycloak:8080/auth/"
        - name: KEYCLOAK_REALM
          value: precmed
        - name: KEYCLOAK_CLIENT
          value: minio
        - name: KEYCLOAK_ROLES_ADMIN_USERNAME
          valueFrom:
              secretKeyRef:
                name: keycloak-roles-admin
                key: USERNAME
        - name: KEYCLOAK_ROLES_ADMIN_PASSWORD
          valueFrom:
              secretKeyRef:
                name: keycloak-roles-admin
                key: PASSWORD
        - name: GROUPS
          value: '["bioacademy-gr", "certh-gr", "eie-gr", "fleming-gr", "forth-gr", "med-uoa-gr", "rrp-demokritos-gr", "uoc-gr"]'
        - name: S3_BIOACADEMY_GR_ACCESS_KEY
          valueFrom:
              secretKeyRef:
                name: s3-bioacademy-gr
                key: access_key
        - name: S3_BIOACADEMY_GR_SECRET_KEY
          valueFrom:
              secretKeyRef:
                name: s3-bioacademy-gr
                key: secret_key
        - name: S3_CERTH_GR_ACCESS_KEY
          valueFrom:
              secretKeyRef:
                name: s3-certh-gr
                key: access_key
        - name: S3_CERTH_GR_SECRET_KEY
          valueFrom:
              secretKeyRef:
                name: s3-certh-gr
                key: secret_key
        - name: S3_EIE_GR_ACCESS_KEY
          valueFrom:
              secretKeyRef:
                name: s3-eie-gr
                key: access_key
        - name: S3_EIE_GR_SECRET_KEY
          valueFrom:
              secretKeyRef:
                name: s3-eie-gr
                key: secret_key
        - name: S3_FLEMING_GR_ACCESS_KEY
          valueFrom:
              secretKeyRef:
                name: s3-fleming-gr
                key: access_key
        - name: S3_FLEMING_GR_SECRET_KEY
          valueFrom:
              secretKeyRef:
                name: s3-fleming-gr
                key: secret_key
        - name: S3_FORTH_GR_ACCESS_KEY
          valueFrom:
              secretKeyRef:
                name: s3-forth-gr
                key: access_key
        - name: S3_FORTH_GR_SECRET_KEY
          valueFrom:
              secretKeyRef:
                name: s3-forth-gr
                key: secret_key
        - name: S3_MED_UOA_GR_ACCESS_KEY
          valueFrom:
              secretKeyRef:
                name: s3-med-uoa-gr
                key: access_key
        - name: S3_MED_UOA_GR_SECRET_KEY
          valueFrom:
              secretKeyRef:
                name: s3-med-uoa-gr
                key: secret_key
        - name: S3_RRP_DEMOKRITOS_GR_ACCESS_KEY
          valueFrom:
              secretKeyRef:
                name: s3-rrp-demokritos-gr
                key: access_key
        - name: S3_RRP_DEMOKRITOS_GR_SECRET_KEY
          valueFrom:
              secretKeyRef:
                name: s3-rrp-demokritos-gr
                key: secret_key
        - name: S3_UOC_GR_ACCESS_KEY
          valueFrom:
              secretKeyRef:
                name: s3-uoc-gr
                key: access_key
        - name: S3_UOC_GR_SECRET_KEY
          valueFrom:
              secretKeyRef:
                name: s3-uoc-gr
                key: secret_key
        ports:
        - containerPort: 8000
        - containerPort: 8001
        volumeMounts:
        - name: iodc-secrets
          mountPath: /etc/iodc_secrets
        - name: pmu-web-app-storage
          mountPath: /app/storage
      initContainers:
      - name: generate-iodc-secret
        image: registry.gitlab.com/precmed/deployment/authentication/generate-iodc-secrets
        env:
        - name: KEYCLOAK_HOST_GLOBAL
          value: "accounts.oncopmnet.gr"
        - name: KEYCLOAK_REALM
          value: precmed
        - name: KEYCLOAK_CLIENT_ID
          value: service
        - name: KEYCLOAK_CLIENT_SECRET
          valueFrom:
            secretKeyRef:
              name: pmu-web-api-iodc-secrets
              key: KEYCLOAK_CLIENT_SECRET
        - name: KEYCLOAK_REDIRECT_URI
          value: "service.oncopmnet.gr"
        - name: SECRETS_DIR
          value: /etc/iodc_secrets/
        volumeMounts:
        - name: iodc-secrets
          mountPath: /etc/iodc_secrets
      volumes:
        - name: iodc-secrets
          emptyDir: {}
        - name: pmu-web-app-storage
          persistentVolumeClaim:
            claimName: pmu-web-app-storage
