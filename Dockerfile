FROM python:3.6-alpine3.9

COPY requirements.txt /app/

WORKDIR /app

RUN pip install -r requirements.txt

RUN apk add --no-cache \
    yaml-dev \
    zip

COPY routines /app/routines
COPY templates /app/templates
COPY app.py app_rest.py config.json run_flask.sh /app/

RUN chmod 755 /app/run_flask.sh

EXPOSE 8000
EXPOSE 8001

CMD /app/run_flask.sh
