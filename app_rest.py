from flask_restful import Api
from routines.cancel import Cancel
from routines.status import Status
from routines.outputs import Outputs
from flask import Flask
from routines.workflow import SubmitWorkflow
from routines.list_workflows import ListWorkflows
from routines.upload_vcf import UploadVCF
import logging
import os
from flask_oidc import OpenIDConnect

app_rest = Flask(__name__)

app_rest.config.update({
    'SECRET_KEY': os.environ.get('OIDC_SECRET'),
    'TESTING': True,
    'DEBUG': True,
    'OIDC_CLIENT_SECRETS': os.environ.get('IODC_CLIENT_SECRETS_PATH'),
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_OPENID_REALM': os.environ.get('KEYCLOAK_REALM'),
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token'
})
oidc = OpenIDConnect(app_rest)

api = Api(app_rest)


@app_rest.route('/workflow', methods=['POST'])
@oidc.accept_token(require_token=True)
def workflow_post():
    submit_workflow = SubmitWorkflow(lambda x: x + "\n", oidc.user_getfield('sub'))
    r = submit_workflow.post()
    return r


@app_rest.route('/status', methods=['POST'])
@oidc.accept_token(require_token=True)
def status_post():
    status = Status(lambda x: x + "\n", oidc.user_getfield('sub'))
    r = status.post()
    return r


@app_rest.route('/outputs', methods=['POST'])
@oidc.accept_token(require_token=True)
def outputs_post():
    outputs = Outputs(lambda x: x + "\n", oidc.user_getfield('sub'))
    return outputs.post()


@app_rest.route('/cancel', methods=['POST'])
@oidc.accept_token(require_token=True)
def cancel_post():
    cancel = Cancel(lambda x: x + "\n", oidc.user_getfield('sub'))
    return cancel.post()


@app_rest.route('/list_workflows', methods=['GET'])
@oidc.accept_token(require_token=True)
def list_workflow_get():
    workflows = ListWorkflows()
    return workflows.get()

@app_rest.route('/upload_vcf', methods=['POST'])
@oidc.accept_token(require_token=True)
def upload_vcf_post():
    upload = UploadVCF(lambda x: x+"\n", oidc.user_getfield('sub'))
    return upload.post()


logging.basicConfig(filename='errors.log', level=logging.ERROR)

if __name__ == '__main__':
    app_rest.run(debug=True)
