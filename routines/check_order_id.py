from routines.routine import Routine
from flask_restful import Resource
from flask import request
import botocore
import boto3
import json
import logging
import os
import json
from routines.keycloak_admin import KeycloakAdmin
import requests
import re

ka = KeycloakAdmin()
valid_groups = set(json.loads(os.environ.get("GROUPS")))

class CheckOrderId(Routine):

    def __init__(self, handle, user_id):
        super().__init__(user_id)
        self.handle = handle
    def post(self):
        if 'orderId' not in request.form or request.form['orderId'] == "":
            err = "Error: Missing Order Id"
            logging.error(err)
            return err
        self.orderId = request.form['orderId']
        order_ids = super().get_order_ids()
        
        #check if order id is valid
        pattern = "^[0-9]{4}-[0-9]{5}-[0-9]{4}$"
        if not bool(re.match(pattern,self.orderId)):
            err = "Error: Referral number is invalid. It must be of the form XXXX-XXXXX-XXXX where X is any digit"
            logging.error(err)
            return self.handle(err)
        
        if self.orderId in order_ids:
            return self.handle("taken")
        else: 
            return self.handle("not_taken")
        return None

   

