from flask_restful import Resource
from flask import render_template
import json


class ListWorkflows(Resource):

    def __init__(self):
        with open("./config.json", "r") as fp:
            self.config = json.load(fp=fp)

    def get(self):
        workflows = self.config['workflows']
        return render_template('list_workflows.html', workflows=workflows)
