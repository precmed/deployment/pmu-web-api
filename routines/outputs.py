from flask import send_file, after_this_request
from routines.routine import Routine
from datetime import datetime
import subprocess
import botocore
import logging
import shutil
import json
import os
import requests


class Outputs(Routine):
    """
         1. Asks Cromwell about workflow run output locations;
         2. stores these locations as a file on output bucket
         3. downloads the output files from these locations
         4. sets downloaded status to 1
         5. returns files to user
         6. deletes those files
    """

    def __init__(self, handle, user_id):

        super().__init__(user_id)
        self.handle = handle
        self.workflow_id = None
        self.path = None
        self.dir_path_outputs = "./storage/outputs/"
        if not os.path.exists(self.dir_path_outputs):
            os.makedirs(self.dir_path_outputs)

    def post(self):

        err = super().post()
        if err is not None:
            logging.error(err)
            return self.handle(err)

        workflow_id = self.get_workflow_id()
        if workflow_id is None:
            err = "No workflow run with such order referral number"
            logging.error(err)
            return self.handle(err)

        status = super().get_status(workflow_id)

        if status != "Succeeded":
            return self.handle("Can't download: Workflow status: " + status)

        # Invoke Cromwell
        r = requests.get(self.cromwell+'/'+workflow_id+'/outputs')
        if not r.ok:
            err = self.cromwell + " returned " + str(r.status_code)
            logging.error(err)
            return self.handle(err)
        ret_json = r.json()
        print(ret_json)
        if 'status' in ret_json and ret_json['status'] == "fail":
            err = ret_json['message']
            logging.error(err)
            return self.handle(err)

        now = datetime.now()
        timestamp = datetime.timestamp(now)
        self.path = self.dir_path_outputs + self.orderId + "/" + str(timestamp) + "/"
        outputs = ret_json['outputs']
        for output_name in outputs:
            err = self.download_file(outputs[output_name]['location'])
            if err is not None:
                logging.error(err)
                return self.handle(err)

        downloaded_status = super().read_object_content(self.orderId, "downloaded_status")
        if downloaded_status == "0":
            err = super().update_outputs_bucket("downloaded_status_filename", "1")
            if err is not None:
                logging.error(err)
                return self.handle(err)

        zipfile = self.path + "outputs.zip"
        cmd = "zip -m -1 -j " + zipfile + " " + self.path + "*"
        proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
        (_ret, _err) = proc.communicate()

        try:
            @after_this_request
            def remove_file(response):
                try:
                    shutil.rmtree(self.path)
                except Exception as err:
                    print(err)
                    return self.handle(err)
                return response

            return send_file(zipfile, as_attachment=True)
        except Exception as err:
            return self.handle(str(err))

    def download_file(self, location):

        x = location.split("/", 4)
        bucket_name = x[3]
        obj_name = x[4]
        file_name = x[4].split("/")
        try:
            bucket = self.client.Bucket(bucket_name)
            obj = bucket.Object(obj_name)
            if obj is not None:
                if not os.path.exists(self.path):
                    os.makedirs(self.path)
                path = self.path + self.orderId + "." + file_name[-1].split(".", 2)[1]
                bucket.download_file(Filename=path, Key=obj_name)
        except botocore.exceptions.ClientError as err:
            return err
        return None
