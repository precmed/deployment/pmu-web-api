from routines.routine import Routine
import logging


class Cancel(Routine):

    def __init__(self, handle, user_id):
        super().__init__(user_id)
        self.handle = handle
        self.output_bucket_name = self.config["outputs_bucket"]

    def post(self):
        err = super().post()
        if err is not None:
            logging.error(err)
            return self.handle(err)

        workflow_id = super().get_workflow_id()
        if workflow_id is None:
            err = "No workflow run with such order id"
            logging.error(err)
            return self.handle(err)

        err = super().update_outputs_bucket("downloaded_status_filename", "-1")
        if err is not None:
            logging.error(err)
            return self.handle(err)

        return self.handle("Canceled")

