from flask_restful import Resource
from flask import request
import botocore
import boto3
import json
import logging
import os
import json
from routines.keycloak_admin import KeycloakAdmin
import requests

ka = KeycloakAdmin()
valid_groups = set(json.loads(os.environ.get("GROUPS")))

class Routine(Resource):

    def __init__(self, user_id):
        with open("./config.json", "r") as fp:
            self.config = json.load(fp=fp)
        # with open("./metadata.json", "r") as fp:
        #     self.metadata = json.load(fp=fp)
        self.group = ka.get_group(user_id, valid_groups)
        self.cromwell = 'http://workflows-' + str(self.group) + "-cromwell:" + str(os.getenv('CROMWELL_PORT')) + "/api/workflows/v1"
        self.netloc = os.getenv('S3_GATEWAY')

        self.orderId = None
        self.output_bucket_name = self.config["outputs_bucket"]
        self.user_id = user_id
        if self.group:
            access_key, secret_key = self.get_access_keys()
            self.client = boto3.resource('s3',
                                endpoint_url='http://'+self.netloc,
                                aws_access_key_id=access_key,
                                aws_secret_access_key=secret_key)

    def post(self):
        if 'orderId' not in request.form or request.form['orderId'] == "":
            err = "Error: Missing Referral Number"
            logging.error(err)
            return err
        self.orderId = request.form['orderId']

        if not self.group:
            err = "User does not belong to any group"
            logging.error(err)
            return err
        try:
            requests.get(self.cromwell)
        except:
            err =  "Cannot connect to " + self.cromwell
            logging.error(err)
            return err

        return None

    def get_access_keys(self):

        group_caps = self.group.upper().replace("-", "_")
        access_key = os.getenv(f'S3_{group_caps}_ACCESS_KEY')
        secret_key = os.getenv(f'S3_{group_caps}_SECRET_KEY')

        return access_key, secret_key

    def get_workflow_id(self):

        obj_name = self.orderId + "/" + self.config['workflow_id_filename']
        workflow_id = None
        try:
            bucket = self.client.Bucket(self.output_bucket_name)
            objs = list(bucket.objects.filter(Prefix=obj_name))
            if len(objs):
                workflow_id = objs[0].get()["Body"].read().decode('utf-8').strip("\n")
        except botocore.exceptions.ClientError as err:
            return err
        return workflow_id

    def update_outputs_bucket(self, filename, content):

        filename = self.orderId + "/" + self.config[filename]
        try:
            self.client.Object(self.output_bucket_name, filename).put(Body=content)
        except botocore.exceptions.ClientError as err:
            return err
        return None

    def get_status(self, workflow_id):

        # Invoke Cromwell
        
        r = requests.get(self.cromwell+'/'+workflow_id+'/status')
        ret = r.json()
        print(ret)
        if 'status' in ret and ret['status'] == "fail":
            err = ret['message']
            logging.error(err)
            return err

        return ret["status"]

    def get_order_ids(self):

        bucket = self.client.Bucket(self.output_bucket_name)
        # Get all existing order_ids
        order_ids = set()
        for file in bucket.objects.all():
            print(file)
            x = file.key.split("/", 2)
            order_ids.add(x[0])
        return order_ids

    def read_object_content(self, order_id, file):
        bucket = self.client.Bucket(self.output_bucket_name)
        obj = bucket.Object(order_id + "/" + file)
        if obj is not None:
            return obj.get()["Body"].read().decode('utf-8').strip("\n")


