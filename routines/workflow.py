from typing import Pattern
from routines.routine import Routine
import uuid
from yaml import load, dump, FullLoader
from flask import request, render_template, jsonify
import requests
import botocore
import logging
import json
import os
import requests
import urllib.parse
import json
import re 


#Finds the nodes in a loaded yaml that have key name that matches the parameter match and then
#swaps the hostname in the path that is contained in their value with that of parameter netloc
def change_host_value(yaml_node, match , netloc):
    if type(yaml_node) == list:
        for item in yaml_node:
            change_host_value(item, match ,netloc)
    else:
        for key, value in yaml_node.items():
            if type(value) == dict:
                change_host_value(value, match ,netloc)
            elif type(value) == list:
                for item in value:
                    change_host_value(item, match ,netloc)
            elif key == match:
                path=yaml_node[key]
                parsed_path_list = list(urllib.parse.urlparse(path))
                parsed_path_list[1] = netloc
                yaml_node[key] = urllib.parse.urlunparse(parsed_path_list)



class SubmitWorkflow(Routine):

    def __init__(self, handle, user_id, username, name):

        super().__init__(user_id)
        self.handle = handle
        self.workflows_tank = self.config['workflows_tank']
        self.workflows = self.config['workflows']
        self.workflow_path = None
        self.workflow_type = None
        self.workflow = None
        self.uuid = str(uuid.uuid4())
        self.inputs = None
        self.input_names = {}
        self.input_bucket_name = self.config["inputs_bucket"]
        self.dir_path = "./storage/"
        self.local_inputs_path = self.dir_path + "inputs"
        if not os.path.exists(self.local_inputs_path):
            os.makedirs(self.local_inputs_path)
        self.yaml_input_path = None
        self.metadata = {}
        self.workflow_id = ""
        self.username = username
        self.name = name

    def post(self):

        err = super().post()
        if err is not None:
            logging.error(err)
            return self.handle(err)


        order_ids = super().get_order_ids()
        if self.orderId in order_ids:
            err = "Error: Referral number already taken"
            logging.error(err)
            return self.handle(err)
        
        #check if order id is valid
        pattern = "^[0-9]{4}-[0-9]{5}-[0-9]{4}$"
        if not bool(re.match(pattern,self.orderId)):
            err = "Error: Referral number is invalid. It must be of the form XXXX-XXXXX-XXXX where X is any digit"
            logging.error(err)
            return self.handle(err)

        # Validate the existence of inputs
        if 'type' not in request.form:
            err = "Error: Missing workflow type"
            logging.error(err)
            return self.handle(err)

        self.workflow_type = request.form['type']
        print("workflow type", self.workflow_type)
        self.workflow = self.workflows[self.workflow_type]
        if self.workflow is None:
            err = "Error: No such type of workflow"
            logging.error(err)
            return self.handle(err)

        self.workflow_path = self.workflows_tank + "/" + \
                             self.workflow["repository"] + \
                             "/raw/" + self.workflow["ref"] + "/"

        # ...................................................
        self.inputs = {}
        # todo: massage this ; support urls too for inputs
        if self.workflow_type == "somatic-illumina-solid":
            if 'somatic_illumina_solid_fastq1' not in request.files \
                    or 'somatic_illumina_solid_fastq2' not in request.files \
                    or not request.files['somatic_illumina_solid_fastq1'] \
                    or not request.files['somatic_illumina_solid_fastq2']:
                err = "Error: Missing fastq/s"
                logging.error(err)
                return self.handle(err)

        elif self.workflow_type == "somatic-torrent-solid":
            if 'somatic_torrent_solid_bam' not in request.files or not request.files['somatic_torrent_solid_bam']:
                err = "Error: Missing bam file"
                logging.error(err)
                return self.handle(err)

        elif self.workflow_type == "somatic-illumina-hematologic":
            if 'somatic_illumina_hematologic_fastq1' not in request.files \
                    or 'somatic_illumina_hematologic_fastq2' not in request.files \
                    or not request.files['somatic_illumina_hematologic_fastq1'] \
                    or not request.files['somatic_illumina_hematologic_fastq2']:
                err = "Error: Missing fastq/s"
                logging.error(err)
                return self.handle(err)

        elif self.workflow_type == "somatic-torrent-hematologic":
            if 'somatic_torrent_hematologic_bam' not in request.files or not request.files['somatic_torrent_hematologic_bam']:
                err = "Error: Missing bam file"
                logging.error(err)
                return self.handle(err)

        elif self.workflow_type == "hereditary-cancer":
            if 'hereditary_cancer_fastq1' not in request.files \
                    or 'hereditary_cancer_fastq2' not in request.files \
                    or not request.files['hereditary_cancer_fastq1'] \
                    or not request.files['hereditary_cancer_fastq2']:
                err = "Error: Missing fastq/s"
                logging.error(err)
                return self.handle(err)

        self.inputs = dict((k,v) for k, v in request.files.items() if v)
        # ...................................................

        try:
            for k, f in self.inputs.items():
                f.save(os.path.join(self.local_inputs_path, self.uuid + '_' + k + '_' + f.filename))
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        # Upload input files to pmu-platform
        err = self.upload_files()
        if err is not None:
            logging.error(err)
            return self.handle(err)

        # Build the correct yaml-workflow-input
        err = self.build_yml_input()
        if err is not None:
            logging.error(err)
            return self.handle(err)

        # Invoke Cromwell
        files = {
            'workflowUrl': (None, self.workflow_path + self.workflow['cwl']),
            'workflowInputs': (self.yaml_input_path, open(self.yaml_input_path, 'rb')),
            'workflowType': (None, self.workflow["specification_language"]),
            'workflowTypeVersion': self.workflow["workflow_version"]
        }

        r = requests.post(self.cromwell, files=files)
        if not r.ok:
            err = self.cromwell + " returned " + str(r.status_code)
            logging.error(err)
            return self.handle(err)
        ret = r.json()
        print(ret)
        if 'status' in ret and ret['status'] == "fail":
            err = ret['message']
            logging.error(err)
            return self.handle(err)

        # Create "downloaded-status" in outputs_bucket
        err = super().update_outputs_bucket("downloaded_status_filename", "0")
        if err is not None:
            logging.error(err)
            return self.handle(err)

        # Get workflow's id and status from cromwell's response
        self.workflow_id = ret["id"]
        workflow_status = ret["status"]
        # Change the status message to a more detailed one.
        if workflow_status == "Submitted":
            workflow_status = "Workflow is submitted. You can check its status at Status tab."
        # Write "mapped_workflow_id" in workflow-outputs folder
        err = super().update_outputs_bucket("workflow_id_filename", self.workflow_id)
        if err is not None:
            logging.error(err)
            return self.handle(err)
        
        # Write metadata file in workflow-outputs folder under order_id
        err = self.update_metadata()
        if err is not None:
            logging.error(err)
            return self.handle(err)


        # Cleaning
        try:
            os.remove(self.yaml_input_path)
            for k, f in self.inputs.items():
                os.remove(self.local_inputs_path + "/" + self.uuid + '_' + k + '_' + f.filename)
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        return self.handle(workflow_status)
    
    def update_metadata(self):
        from datetime import datetime
        now = datetime.now()
        timestamp = datetime.timestamp(now)
        
        self.metadata['workflow-id'] = self.workflow_id
        self.metadata['workflow-type'] = self.workflow_type
        self.metadata['user-id'] = self.user_id
        self.metadata['username'] = self.username
        self.metadata['name'] = self.name
        self.metadata['group']=self.group
        self.metadata['timestamp']=timestamp

        tmp_file = "/tmp/" + str(timestamp) + "_" + self.user_id + "_metadata.json"
        try:
            with open(tmp_file, 'w+') as outfile:
                    json.dump(self.metadata, outfile)
        except Exception as err:
            logging.error(err)
            return self.handle(err)
        try:
                bucket = self.client.Bucket(self.config["outputs_bucket"])
                bucket.upload_file(Filename=tmp_file, Key=self.orderId+"/metadata.json")
        except botocore.exceptions.ClientError as err:
                logging.error(err)
                return self.handle(err)
        
        try:
            os.remove(tmp_file)
        except Exception as err:
            logging.error(err)
            return self.handle(err)
        
        return None

    def build_yml_input(self):

        """
        Retrieves the yml file from 'workflow_yml' location,
        de-serializes it, adds the correct input paths,
        then serializes it again and returns it
        """

        path = "s3://" + self.netloc + "/" + self.input_bucket_name + "/"

        workflow_yml = self.workflow['yml']

        try:
                r = requests.get(self.workflow_path + workflow_yml)
                read = r.content
                data = load(read, Loader=FullLoader)

                if self.workflow_type == "somatic-illumina-solid":
                    data['reads'][0]['path'] = path + self.input_names['somatic_illumina_solid_fastq1']
                    data['reads'][1]['path'] = path + self.input_names['somatic_illumina_solid_fastq2']
                elif self.workflow_type == "somatic-torrent-solid":
                    data['input_ubam']['path'] = path + self.input_names['somatic_torrent_solid_bam']
                    if 'somatic_torrent_solid_bed' in self.input_names:
                        data['bed_file']['path'] = self.input_names['somatic_torrent_solid_bed']
                    if 'somatic_torrent_solid_parameters' in self.input_names:
                        data['parameters']['path'] = self.input_names['somatic_torrent_solid_parameters']
                elif self.workflow_type == "somatic-illumina-hematologic":
                    data['reads'][0]['path'] = path + self.input_names['somatic_illumina_hematologic_fastq1']
                    data['reads'][1]['path'] = path + self.input_names['somatic_illumina_hematologic_fastq2']
                elif self.workflow_type == "somatic-torrent-hematologic":
                    data['input_ubam']['path'] = path + self.input_names['somatic_torrent_hematologic_bam']
                    if 'somatic_torrent_hematologic_bed' in self.input_names:
                        data['bed_file']['path'] = self.input_names['somatic_torrent_hematologic_bed']
                    if 'somatic_torrent_hematologic_parameters' in self.input_names:
                        data['parameters']['path'] = self.input_names['somatic_torrent_hematologic_parameters']
                elif self.workflow_type == "hereditary-cancer":
                    data['reads'][0]['path'] = path + self.input_names['hereditary_cancer_fastq1']
                    data['reads'][1]['path'] = path + self.input_names['hereditary_cancer_fastq2']
                data['order_id'] = self.orderId

                change_host_value(data, "path", self.netloc)

                self.yaml_input_path = self.dir_path + "_" + self.uuid + "_" + workflow_yml
                with open(self.yaml_input_path, 'w+') as write:
                    dump(data, write)

        except Exception as err:
            logging.error(err)
            if not isinstance(err, str):
                err = "Oops something went wrong. Try again later!"
            return self.handle(err)

        return None

    def upload_files(self):

        """
            Uploads input files to s3
        """
        # prefix = ""
        for k, f in self.inputs.items():

            file_name = self.uuid + '_' + k + '_' + f.filename
            self.input_names[k] = file_name

            try:
                bucket = self.client.Bucket(self.input_bucket_name)
                bucket.upload_file(Filename=os.path.join(self.local_inputs_path, file_name), Key=file_name)

            except botocore.exceptions.ClientError as err:
                logging.error(err)
                return self.handle(err)
        return None
