from routines.routine import Routine
# from yaml import CLoader as Loader
from flask import request
import botocore
import logging
import json
import os

class UploadVCF(Routine):

    def __init__(self, handle, user_id):
        super().__init__(user_id)
        self.handle = handle
        self.output_bucket_name = self.config["upload_vcf_bucket"]
        self.dir_path = "./storage/"
        self.local_inputs_path = self.dir_path + "inputs"

    
    def is_vcf_gzipped(self, vcffile):
        import re
        import gzip
        try:
            with open(vcffile, 'rb') as fp:
                buf = fp.read(80)
                zipped = False
                if (buf[0] == 0x1F and
                    buf[1] == 0x8B and
                    buf[2] == 0x8):
                    zipped = True
                    with gzip.open(vcffile, 'rb') as gf:
                        buf = gf.readline()
                line = buf.decode("utf-8")
                m = re.match(r"##fileformat=VCFv\d\.\d", line)
                return (m is not None, zipped)
        except:
            return (False, False)

    
    def post(self):

        err = super().post()
        if err is not None:
            logging.error(err)
            return self.handle(err)
        
        file_key = self.orderId+".vcf.gz"
        bucket = self.client.Bucket(self.output_bucket_name)
        try:
            for file in bucket.objects.all():
                if file_key == file.key:
                    err = "Error: Referral Number already taken"
                    logging.error(err)
                    return self.handle(err)
        except Exception as err:
            logging.error(err)
            return self.handle(err)    

        file = request.files['file']
        if not file:
            err = "Error: Missing file"
            logging.error(err)
            return self.handle(err)
        local_filename = os.path.join(self.local_inputs_path, file.filename)
        try:
            file.save(local_filename)
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        is_vcf, is_gzipped = self.is_vcf_gzipped(local_filename)
        if not is_vcf:
            err = "Error: This file is not a valid VCF file"
            logging.error(err)
            return self.handle(err)
        if not is_gzipped:
            try:
                import gzip
                f_in = open(local_filename, 'rb')
                zipped_local_filename = local_filename+'.gz'
                f_out = gzip.GzipFile(zipped_local_filename, 'wb')
                f_out.write(f_in.read())
                f_out.close()
                f_in.close()
                os.remove(local_filename)
                local_filename = zipped_local_filename
            except Exception as err:
                logging.error(err)
                return self.handle(err)

        try:
            bucket.upload_file(Filename=local_filename, Key=file_key)
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        try:
            os.remove(local_filename)
        except Exception as err:
            logging.error(err)
            return self.handle(err)

        return self.handle("Successfully uploaded")
        

