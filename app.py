from flask_restful import Api
from routines.cancel import Cancel
from routines.status import Status
from routines.outputs import Outputs
from routines.check_order_id import CheckOrderId
from routines.upload_vcf import UploadVCF
from flask import Flask, render_template, redirect, request
from routines.workflow import SubmitWorkflow
from routines.list_workflows import ListWorkflows
import logging
import os
from flask_oidc import OpenIDConnect
import urllib.parse

app = Flask(__name__)

app.config.update({
    'SECRET_KEY': os.environ.get('OIDC_SECRET'),
    'TESTING': True,
    'DEBUG': True,
    'OIDC_CLIENT_SECRETS': os.environ.get('IODC_CLIENT_SECRETS_PATH'),
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_OPENID_REALM': os.environ.get('KEYCLOAK_REALM'),
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token'
})
oidc = OpenIDConnect(app)

api = Api(app)

if os.environ.get('VAST_URL') is not None:
    vast_url = "https://"+os.environ.get("VAST_URL")
else:
    vast_url=None

@app.route('/')
def index():
    return render_template('home.html', vast_url=vast_url)


@app.route('/launch', methods=['GET'])
@oidc.require_login
def launch_get():
    return render_template('launch.html', vast_url=vast_url)


@app.route('/launch', methods=['POST'])
@oidc.require_login
def launch_post():
    submit_workflow = SubmitWorkflow(lambda x: x, oidc.user_getfield('sub'), oidc.user_getfield('preferred_username'), oidc.user_getfield('name'))
    r = submit_workflow.post()
    return r

@app.route('/status', methods=['GET'])
@oidc.require_login
def status_get():
    return render_template('status.html', vast_url=vast_url)


@app.route('/status', methods=['POST'])
@oidc.require_login
def status_post():
    status = Status(lambda x: x, oidc.user_getfield('sub'))
    r = status.post()
    return render_template('status.html', status=r, vast_url=vast_url)


@app.route('/outputs', methods=['GET'])
@oidc.require_login
def outputs_get():
    return render_template('outputs.html', vast_url=vast_url)


@app.route('/outputs', methods=['POST'])
@oidc.require_login
def outputs_post():
    outputs = Outputs(lambda x: render_template('outputs.html', status=x, vast_url=vast_url), oidc.user_getfield('sub'))
    return outputs.post()


@app.route('/cancel', methods=['GET'])
@oidc.require_login
def cancel_get():
    return render_template('cancel.html', vast_url=vast_url)


@app.route('/cancel', methods=['POST'])
@oidc.require_login
def cancel_post():
    cancel = Cancel(lambda x: x, oidc.user_getfield('sub'))
    r = cancel.post()
    return render_template('cancel.html', status=r, vast_url=vast_url)


@app.route('/list_workflows', methods=['GET'])
@oidc.require_login
def list_workflow_get():
    workflows = ListWorkflows()
    return workflows.get()

@app.route('/get_token', methods=['GET'])
@oidc.require_login
def get_token():
    token = oidc.get_access_token()
    if not token:
        oidc.logout()
        return render_template('home.html')
    return render_template('authenticate.html', token=token, vast_url=vast_url)

@app.route('/logout', methods=['GET'])
def logout():
    oidc.logout()
    return redirect(oidc.client_secrets['logout_uri']+'?'+
        urllib.parse.urlencode({'redirect_uri':request.host_url}))


@app.route('/upload_vcf', methods=['GET'])
@oidc.require_login
def upload_vcf_get():
    return render_template('upload_vcf.html', vast_url=vast_url)

@app.route('/checkorderid', methods=['POST'])
@oidc.require_login
def check_orderid_post():
    check_orderid = CheckOrderId(lambda x: x, oidc.user_getfield('sub'))
    r = check_orderid.post()
    if not isinstance(r, str):
        r = "Oops something went wrong. Try again later!"
    return r

@app.route('/upload_vcf', methods=['POST'])
@oidc.require_login
def upload_vcf_post():
    upload = UploadVCF(lambda x: x, oidc.user_getfield('sub'))
    r = upload.post()
    return render_template('upload_vcf.html', status=r, vast_url=vast_url)

logging.basicConfig(filename='errors.log', level=logging.ERROR, format='[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s', datefmt='%m-%d %H:%M:%S')

if __name__ == '__main__':
    app.run(debug=True)
