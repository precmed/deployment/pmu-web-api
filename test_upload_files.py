from routines.workflow import SubmitWorkflow

submit_workflow = SubmitWorkflow()

file_path1 = "./test.txt"
file_path2 = "./test1.txt"

submit_workflow.workflow = submit_workflow.workflows["somatic"]
s3_filepath1, s3_filepath2 = submit_workflow.upload_files(file_path1, "testing1.txt", file_path2, "testing2 .txt")

print("FilePath1: ", s3_filepath1)
print("FilePath2: ", s3_filepath2)

yaml_input_path = submit_workflow.build_yml_input(s3_filepath1, s3_filepath2)
print("Yaml Input Path:", yaml_input_path)
