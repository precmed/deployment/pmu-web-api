from routines.workflow import SubmitWorkflow

submit_workflow = SubmitWorkflow()

file_path1 = "bubbles1"
file_path2 = "bubbles2"

submit_workflow.workflow = submit_workflow.workflows["somatic"]
yaml_input_path = submit_workflow.build_yml_input(file_path1, file_path2)

print("Yaml Input Path:", yaml_input_path)
